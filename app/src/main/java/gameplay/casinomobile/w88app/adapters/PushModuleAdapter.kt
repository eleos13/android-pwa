package gameplay.casinomobile.w88app.adapters

import android.content.Context
import com.os.operando.garum.models.PrefModel
import gameplay.casinomobile.pushlibrary.push.PushModule
import gameplay.casinomobile.pushlibrary.push.data.models.AnalyticsEvent
import gameplay.casinomobile.w88app.interfaces.PushModuleInterface

class PushModuleAdapter : PushModuleInterface {

    override fun subscribeNoLogin(context: Context) {
        PushModule.subscribeNoLogin(context)
    }

    override fun subscribe(
        context: Context,
        operatorId: String,
        userId: String,
        username: String,
        riskId: String,
        currency: String,
        language: String,
        token: String
    ) {
        PushModule.subscribe(context, operatorId, userId, username, riskId, currency, language, token)
    }

    override fun startListening(context: Context) {
        PushModule.startListening(context)
    }

    override fun init(context: Context, baseUrl: String?, versionName: String, vararg classes: Class<out PrefModel>) {
        PushModule.initInstance(context = context, versionName = versionName, baseUrl = baseUrl, classes = *classes)
    }

    override fun startTimingEventActivity(context: Context, eventName: String) {
        PushModule.startTimingEventActivity(context, eventName)
    }

    override fun endTimingEventActivity(context: Context, eventName: String) {
        PushModule.endTimingEventActivity(context, eventName)
    }

    override fun postClickEventActivity(context: Context, viewLabel: String) {
        PushModule.logDeviceActivity(context, AnalyticsEvent.getViewClickedEvent(viewLabel))
    }

    override fun postScreenEventActivity(context: Context, screenName: String, visibility: Boolean) {
        PushModule.logDeviceActivity(context, AnalyticsEvent.getScreenEvent(screenName, visibility))
    }

    override fun postVisibilityEventActivity(context: Context, viewLabel: String, visibility: Boolean) {
        PushModule.logDeviceActivity(context, AnalyticsEvent.getVisibilityChangedEvent(visibility, viewLabel))
    }

    override fun registerNotificationEvent(context: Context, notifUuid: String, type: String) {
        PushModule.trackNotification(context, notifUuid, type)
    }

    override fun getRegisterNotificationIntent(context: Context, notifUuid: String, type: String) =
        PushModule.trackNotificationIntent(context, notifUuid, type)

    override fun setNotificationDismissalSchedule(
        context: Context,
        dismissAt: Long,
        notificationTag: String,
        notificationId: Int
    ) {
        PushModule.scheduleNotificationDismissal(context, notificationTag, dismissAt, notificationId)
    }

    override fun forceCrash() {
        PushModule.forceCrash()
    }

    override fun syncNotificationSettingsUp(context: Context) {
        PushModule.syncUpNotifSettings(context)
    }

    override fun syncNotificationSettingsDown(context: Context) {
        PushModule.syncDownNotifSettings(context)
    }

    override fun getAlarmRestarterPendingIntent(context: Context) = PushModule.getAlarmRestarterPendingIntent(context)

    override fun logCrash(context: Context, crashLogs: String) {
        PushModule.logCrash(context, crashLogs)
    }

}