package gameplay.casinomobile.w88app

import android.app.Application
import androidx.room.Room
import com.chibatching.kotpref.Kotpref
import com.cloudflare.api.CFMobile
import com.downloader.PRDownloader
import com.downloader.PRDownloaderConfig
import com.facebook.stetho.Stetho
import com.google.gson.GsonBuilder
import gameplay.casinomobile.w88app.adapters.PushModuleAdapter
import gameplay.casinomobile.w88app.data.local.AppDatabase
import gameplay.casinomobile.w88app.data.local.ConfigDataManager
import gameplay.casinomobile.w88app.data.remote.NativeService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.android.startKoin
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class W88Application : Application() {

    val w88liteModule = module {
        single {
            CFMobile.createRetrofit(
                Retrofit.Builder().baseUrl("${BuildConfig.defaultDomain}/")
                    .client(
                        OkHttpClient.Builder()
                            .addInterceptor(HttpLoggingInterceptor().apply {
                                level = HttpLoggingInterceptor.Level.BODY
                            }).build()
                    )
                    .addConverterFactory(
                        GsonConverterFactory.create(GsonBuilder().excludeFieldsWithoutExposeAnnotation().create())
                    )
            ).create(NativeService::class.java)
        }
        single {
            ConfigDataManager(get())
        }
        single {
            PushModuleAdapter().apply {
                init(this@W88Application, BuildConfig.BASE_URL, BuildConfig.VERSION_NAME)
            }
        }
        single {
            Room.databaseBuilder(androidApplication(), AppDatabase::class.java, "config.db")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build()
        }
    }

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
        startKoin(this, listOf(w88liteModule))
        PRDownloader.initialize(applicationContext, PRDownloaderConfig.newBuilder().setDatabaseEnabled(true).build())
        Kotpref.init(applicationContext)
    }
}