package gameplay.casinomobile.pwa

import android.content.Context
import android.content.Intent
import android.os.Build
import android.text.TextUtils
import android.util.Log
import android.webkit.WebResourceResponse
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import gameplay.casinomobile.w88app.PWADownloaderService
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.IOException
import java.util.*

class WebviewResourceMappingHelper private constructor() {
    private var localAssetMapModelList: List<LocalAssetMapModel>? = null
    val overridableExtensions: List<String> =
        ArrayList(Arrays.asList("js", "css", "png", "jpg", "woff", "ttf", "eot", "ico"))
    private val downloadQueue = HashSet<String>()

    private val localAssetList: List<LocalAssetMapModel>
        get() {
            var localAssetMapModelList: MutableList<LocalAssetMapModel> = ArrayList()
            val pageData: String? = null
            if (pageData != null) {
                val listType = object : TypeToken<ArrayList<LocalAssetMapModel>>() {

                }.getType()
                localAssetMapModelList = Gson().fromJson(pageData, listType)
            }
            return localAssetMapModelList
        }

    fun queueForDownload(url: String) {
        Log.d("PWADownloaderService", "adding to queue: $url")
        downloadQueue.add(url)
    }

    fun startDownload(context: Context) {
        Log.d("PWADownloaderService", "initiating download for ${downloadQueue.toTypedArray().size} items")
        val i = Intent(context, PWADownloaderService::class.java)
        i.putExtra("urls", downloadQueue.toTypedArray())
        context.startService(i)
    }

    fun getLocalAssetPath(url: String): String? {
        if (TextUtils.isEmpty(url)) {
            return ""
        }
        if (localAssetMapModelList == null) {
            localAssetMapModelList = localAssetList
        }
        if (localAssetMapModelList != null && localAssetMapModelList!!.size > 0) {
            for (localAssetMapModel in localAssetMapModelList!!) {
                if (localAssetMapModel.url == url) {
                    return localAssetMapModel.asset_url
                }
            }
        }
        return ""
    }

    fun getFileExt(fileName: String): String {
        return fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length)
    }

    fun getMimeType(fileExtension: String): String {
        var mimeType = ""
        when (fileExtension) {
            "css" -> mimeType = "text/css"
            "js" -> mimeType = "text/javascript"
            "png" -> mimeType = "image/png"
            "jpg" -> mimeType = "image/jpeg"
            "ico" -> mimeType = "image/x-icon"
            "woff", "ttf", "eot" -> mimeType = "application/x-font-opentype"
        }
        return mimeType
    }

    private inner class LocalAssetMapModel {
        internal var url: String? = null
        internal var asset_url: String? = null
    }

    companion object {
        private var instance: WebviewResourceMappingHelper? = null

        fun getInstance(): WebviewResourceMappingHelper {
            if (instance == null) {
                instance = WebviewResourceMappingHelper()
            }
            return instance!!
        }

        @Throws(IOException::class)
        fun getWebResourceResponseFromAsset(
            context: Context,
            assetPath: String,
            mimeType: String,
            encoding: String
        ): WebResourceResponse {
            val inputStream = context.getAssets().open(assetPath)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val statusCode = 200
                val reasonPhase = "OK"
                val responseHeaders = HashMap<String, String>()
                responseHeaders["Access-Control-Allow-Origin"] = "*"
                return WebResourceResponse(mimeType, encoding, statusCode, reasonPhase, responseHeaders, inputStream)
            }
            return WebResourceResponse(mimeType, encoding, inputStream)
        }

        @Throws(FileNotFoundException::class)
        fun getWebResourceResponseFromFile(file: File, mimeType: String, encoding: String): WebResourceResponse {
            val fileInputStream = FileInputStream(file)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val statusCode = 200
                val reasonPhase = "OK"
                val responseHeaders = HashMap<String, String>()
                responseHeaders["Access-Control-Allow-Origin"] = "*"
                return WebResourceResponse(
                    mimeType,
                    encoding,
                    statusCode,
                    reasonPhase,
                    responseHeaders,
                    fileInputStream
                )
            }
            return WebResourceResponse(mimeType, encoding, fileInputStream)
        }
    }
}