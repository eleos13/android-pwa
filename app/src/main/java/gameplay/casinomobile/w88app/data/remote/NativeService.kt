package gameplay.casinomobile.w88app.data.remote

import gameplay.casinomobile.w88app.data.models.PrecacheManifest
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url

interface NativeService {
    @GET
    fun fetchManifest(@Url url: String) : Call<PrecacheManifest>

    @GET
    fun downloadFileWithDynamicUrlSync(@Url fileUrl: String): Call<ResponseBody>
}