package gameplay.casinomobile.w88app.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import gameplay.casinomobile.w88app.data.models.ConfigData

@Database(entities = [ConfigData::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun configDataDao(): ConfigDataDao
}
