package gameplay.casinomobile.w88app.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import gameplay.casinomobile.w88app.data.models.ConfigData

@Dao
interface ConfigDataDao {

    @Query("SELECT data FROM config_data WHERE config_key = :key")
    fun getItem(key: String): String?

    @Insert
    fun setItem(configData: ConfigData): Long

    @Query("DELETE from config_data WHERE config_key = :key")
    fun removeItem(key: String): Int
}