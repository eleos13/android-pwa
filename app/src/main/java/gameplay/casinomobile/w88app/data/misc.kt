package gameplay.casinomobile.w88app.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ContainerArgs(val url: String, val showFab: Boolean, val containerId: String, val preloadMaxRetry: Int) : Parcelable

@Parcelize
data class GameArgs(val url: String, val showFab: Boolean, val forceDashboard: Boolean, val removeNativeBinding: Boolean) : Parcelable