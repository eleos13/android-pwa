package gameplay.casinomobile.w88app.data.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import gameplay.casinomobile.w88app.BuildConfig

data class PrecacheManifest(
    @Expose @SerializedName("injectConfig")
    val injectConfig: InjectConfig
) {
    data class InjectConfig(
        @Expose @SerializedName("preloadMaxRetry")
        val preloadMaxRetry: Int? = null,
        @Expose @SerializedName("timeoutBeforeRequiringBio")
        val timeoutBeforeRequiringBio: Long? = null,
        @Expose @SerializedName("androidBuild")
        val androidBuild: String? = null,
        @Expose @SerializedName("lazyLoader")
        val lazyLoader: Boolean? = null,
        @Expose @SerializedName("splashScreenDisplay")
        val splashScreenDisplay: Boolean? = null,
        @Expose @SerializedName("useNativeLogin")
        val useNativeLogin: Boolean? = null,
        @Expose @SerializedName("membersite_domain")
        val memberSiteDomain: String? = null,
        @Expose @SerializedName("membersite_domain_zh-cn")
        val memberSiteDomainZhCn: String? = null,
        @Expose @SerializedName("membersite_domain_en-us")
        val memberSiteDomainEnUs: String? = null,
        @Expose @SerializedName("membersite_domain_id-id")
        val memberSiteDomainIdId: String? = null,
        @Expose @SerializedName("membersite_domain_ja-jp")
        val memberSiteDomainJaJp: String? = null,
        @Expose @SerializedName("membersite_domain_km-kh")
        val membersiteDomainKmKh: String? = null,
        @Expose @SerializedName("membersite_domain_ko-kr")
        val membersiteDomainKoKr: String? = null,
        @Expose @SerializedName("membersite_domain_th-th")
        val membersiteDomainThTh: String? = null,
        @Expose @SerializedName("membersite_domain_vi-vn")
        val membersiteDomainViVn: String? = null,
        @Expose @SerializedName("jsonVersion")
        val jsonVersion: JsonVersion? = null
    ) {
        data class JsonVersion(
            @Expose @SerializedName("version")
            val version: Int? = null,
            @Expose @SerializedName("versionName")
            val versionName: String? = null,
            @Expose @SerializedName("required")
            val required: Boolean? = null,
            @Expose @SerializedName("file")
            val file: String? = null,
            @Expose @SerializedName("description")
            val description: String? = null
        )
    }
}