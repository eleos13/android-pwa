package gameplay.casinomobile.w88app.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "config_data")
data class ConfigData(@ColumnInfo(name="config_key") var key: String, @ColumnInfo(name="data") var data: String?) {
    @PrimaryKey(autoGenerate = true)
    var uid: Int = 0
}