package gameplay.casinomobile.w88app.data.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ContainerInfo(@Expose @SerializedName("id") val containerId: String,
                         @Expose @SerializedName("fullPath") val fullPath: String)
