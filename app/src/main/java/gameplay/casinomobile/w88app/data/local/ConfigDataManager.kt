package gameplay.casinomobile.w88app.data.local

import android.content.Context
import gameplay.casinomobile.w88app.data.models.ConfigData

class ConfigDataManager(private var appDatabase: AppDatabase) {

    fun setItem(key: String, data: String) {
        appDatabase.configDataDao().setItem(ConfigData(key = key, data = data))
    }

    fun getItem(key: String): String? {
        return appDatabase.configDataDao().getItem(key)
    }

    fun removeItem(key: String) {
       appDatabase.configDataDao().removeItem(key)
    }
}