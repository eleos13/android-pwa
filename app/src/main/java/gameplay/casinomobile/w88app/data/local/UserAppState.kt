package gameplay.casinomobile.w88app.data.local

import com.chibatching.kotpref.KotprefModel

object UserAppState : KotprefModel() {
    var exitAppTimestamp by longPref(default = 0L)
}