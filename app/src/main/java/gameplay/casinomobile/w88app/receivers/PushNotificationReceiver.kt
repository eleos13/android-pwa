package gameplay.casinomobile.w88app.receivers

import android.content.Context
import gameplay.casinomobile.pushlibrary.push.data.models.PushNotif
import gameplay.casinomobile.pushlibrary.push.receivers.PushReceiver
import gameplay.casinomobile.w88app.showPushNotification

class PushNotificationReceiver : PushReceiver() {

    companion object {
        val TAG = PushNotificationReceiver::class.java.simpleName
    }

    override fun shouldNotify(context: Context, pushNotif: PushNotif) = true

    override fun notify(context: Context, pushNotif: PushNotif, pushId: String) {
//        context.showPushNotification(pushNotif, pushId)
    }
}