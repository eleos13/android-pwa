package gameplay.casinomobile.w88app.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import gameplay.casinomobile.w88app.interfaces.NetworkLostAware

class ConnectivityReceiver(val networkLostAware: NetworkLostAware) : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val cm = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if(cm.activeNetworkInfo!=null && cm.activeNetworkInfo.isConnected) {
            networkLostAware.connectionRestored()
        } else {
            networkLostAware.connectionLost()
        }
    }
}