package gameplay.casinomobile.w88app.interfaces

abstract class PWAJSBindingInterface {
    public abstract fun preloadContainers(stringifiedContainerObjects: String, showFab: Boolean)
    public abstract fun preloadContainers(stringifiedContainerObjects: String)
    public abstract fun loadContainer(containerId: String)
    public abstract fun openInContainer(link: String, containerId: String, showFab: Boolean, forceDashboard: Boolean? = false, removeNativeBinding: Boolean? = false)
    public abstract fun showDashboard()
    public abstract fun shouldPreloadGames() : Boolean
    public abstract fun setItem(key: String, data: String)
    public abstract fun getItem(key: String) : String?
    public abstract fun clearEverything()
    public abstract fun openExternalBrowser(url: String)
    public abstract fun platform() : String
}