package gameplay.casinomobile.w88app.interfaces

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import com.os.operando.garum.models.PrefModel
import gameplay.casinomobile.w88app.BuildConfig

interface PushModuleInterface {
    fun subscribe(context: Context, operatorId: String, userId: String, username: String, riskId: String, currency: String, language: String, token: String)
    fun subscribeNoLogin(context: Context)
    fun startListening(context: Context)
    fun init(context: Context, baseUrl: String? = null, versionName: String = BuildConfig.VERSION_NAME, vararg classes: Class<out PrefModel>)
    fun startTimingEventActivity(context: Context, eventName: String)
    fun endTimingEventActivity(context: Context, eventName: String)
    fun postClickEventActivity(context: Context, viewLabel: String)
    fun postScreenEventActivity(context: Context, screenName: String, visibility: Boolean)
    fun postVisibilityEventActivity(context: Context, viewLabel: String, visibility: Boolean)
    fun registerNotificationEvent(context: Context, notifUuid: String, type: String)
    fun getRegisterNotificationIntent(context: Context, notifUuid: String, type: String): Intent
    fun setNotificationDismissalSchedule(context: Context, dismissAt: Long, notificationTag: String, notificationId: Int)
    fun forceCrash()
    fun syncNotificationSettingsUp(context: Context)
    fun syncNotificationSettingsDown(context: Context)
    fun getAlarmRestarterPendingIntent(context: Context): PendingIntent
    fun logCrash(context: Context, crashLogs: String)
}