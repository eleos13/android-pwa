package gameplay.casinomobile.w88app.interfaces

import android.content.Context
import android.net.ConnectivityManager
import android.view.View

interface NetworkLostAware {
    fun connectionLost()
    fun connectionRestored()
    fun getParentView(): View
    fun Context.isNetworkConnected() = (this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo != null
}