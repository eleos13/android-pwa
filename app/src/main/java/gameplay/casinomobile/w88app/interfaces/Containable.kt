package gameplay.casinomobile.w88app.interfaces

interface Containable {
    fun closeContainerAndRedirect(path: String)
    fun executeCommand(command: String)
    fun onBackPressed()
    fun onContainerReady(containerId: String?)
}