package gameplay.casinomobile.w88app.interfaces

abstract class MobileJSBindingInterface {
    /**
     * Hides this container
     */
    public abstract fun nativeBack()

    /**
     * Opens this url in a separate instance of WebView, inside the app (not external browser)
     */
    public abstract fun onSubmitDeposit(url: String)

    /**
     * Opens this url in an external browser
     */
    public abstract fun openExternalBrowser(url: String)

    /**
     * Launches the app that corresponds to the packageId if installed; otherwise open the downloadUrl in an external browser
     * (this usually translates to the Chrome browser automatically opening the Play Store page for the app
     */
    public abstract fun openApp(packageId: String, downloadUrl: String)

    /**
     * Hides this container
     */
    public abstract fun onNativeBackPressed()

    /**
     * Hides this container, then calls window.redirectTo(path) from the Dashboard webview
     */
    public abstract fun closeContainerAndRedirect(path: String)

    /**
     * Legacy method, currently does nothing but cannot be removed since some games may break if this is missing
     */
    public abstract fun onSlotGameClosed()

    /**
     * Calls window.redirectTo(path) from the Dashboard webview without closing this container
     */
    public abstract fun executeCommand(command: String)
}