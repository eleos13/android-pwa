package gameplay.casinomobile.w88app.interfaces

abstract class WebJSBindingInterface {
    public abstract fun onBackNativeHomePage()
    public abstract fun onBackPressed()
    public abstract fun showPopup(text: String)
}