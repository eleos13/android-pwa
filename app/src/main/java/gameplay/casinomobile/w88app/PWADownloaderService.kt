package gameplay.casinomobile.w88app

import android.app.IntentService
import android.content.Intent
import android.util.Log
import gameplay.casinomobile.w88app.data.remote.NativeService
import okhttp3.ResponseBody
import org.koin.android.ext.android.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*

class PWADownloaderService(name: String? = "") : IntentService(name) {

    val nativeService: NativeService by inject()

    companion object {
        val TAG = PWADownloaderService::class.java.simpleName
    }

    override fun onHandleIntent(intent: Intent?) {
        if (intent!!.hasExtra("urls")) {
            val urls = intent.getStringArrayExtra("urls")
            urls.forEach {
                val file = this@PWADownloaderService.cacheFile(it.split("/").last())
                if (file.exists().not()) {
                    Log.d(TAG, "queueing ${it}")
                    nativeService.downloadFileWithDynamicUrlSync(it).enqueue(object : Callback<ResponseBody> {
                        override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                        }

                        override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>) {
                            if (response.isSuccessful) {
                                writeResponseBodyToDisk(file, response.body()!!)
                                Log.d(TAG, "${file.absolutePath} written to disk.")
                            }
                        }
                    })
                }
            }
        }
    }

    private fun writeResponseBodyToDisk(file: File, body: ResponseBody): Boolean {
        try {
            var inputStream: InputStream? = null
            var outputStream: OutputStream? = null
            try {
                val fileReader = ByteArray(4096)
                val fileSize = body.contentLength()
                var fileSizeDownloaded = 0
                inputStream = body.byteStream()
                outputStream = FileOutputStream(file)

                while (true) {
                    val read = inputStream.read(fileReader)
                    if (read == -1) {
                        break
                    }
                    outputStream.write(fileReader, 0, read)
                    fileSizeDownloaded += read
                }
                outputStream.flush()
                return true
            } catch (e: IOException) {
                return false
            } finally {
                if (inputStream != null) {
                    inputStream.close()
                }
                if (outputStream != null) {
                    outputStream.close()
                }
            }
        } catch (e: IOException) {
            return false
        }
    }
}
