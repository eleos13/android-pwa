package gameplay.casinomobile.w88app

import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Parcelable
import android.provider.Settings
import android.text.Html
import android.text.Spanned
import android.text.TextUtils
import android.util.Log
import android.webkit.URLUtil
import android.webkit.WebView
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.net.toUri
import androidx.fragment.app.FragmentActivity
import com.downloader.Error
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.fondesa.kpermissions.extension.listeners
import com.fondesa.kpermissions.extension.permissionsBuilder
import gameplay.casinomobile.pushlibrary.push.data.models.PushNotif
import gameplay.casinomobile.pushlibrary.push.utils.miscutils.generateNotificationID
import gameplay.casinomobile.pushlibrary.push.utils.miscutils.whenNotNullOrIsTrue
import gameplay.casinomobile.w88app.activities.MainActivity
import gameplay.casinomobile.w88app.adapters.PushModuleAdapter
import gameplay.casinomobile.w88app.fragments.BaseFragment
import org.jetbrains.anko.vibrator
import java.io.File

fun Float.dpToPx() = Math.round(this * Resources.getSystem().displayMetrics.density)

fun String.startsWith(vararg strings: String) = strings.filter { this.startsWith(it, true) }.size > 0

fun String?.buildDashboardUrl() = "${this
    ?: BuildConfig.defaultDomain}/pwa/android-v2/index.html?lazyloader=true" + if (BuildConfig.affiliateID != -1L) "&affiliateID=${BuildConfig.affiliateID}" else "" + "#/"

fun String?.buildPrecacheManifestUrl() =
    "${this ?: BuildConfig.precacheDomain}/android/${BuildConfig.FLAVOR}/precache-manifest.json?v=1"

fun FragmentActivity.requestPermission(
    vararg permissions: String = emptyArray(),
    onGranted: () -> Unit,
    onDenied: () -> Unit
) {
    permissionsBuilder(*permissions).build().apply {
        listeners {
            onAccepted { onGranted.invoke() }
            onDenied { onDenied.invoke() }
        }
        send()
    }
}

const val FRAGMENT_ARG = "fragment_arg"
inline fun <ARG : Parcelable, reified T : BaseFragment<ARG>> newFragment(arg: ARG? = null): T {
    return T::class.java.newInstance().apply { arguments = Bundle().apply { putParcelable(FRAGMENT_ARG, arg) } }
}

fun getDownloadDirectory() =
    File("${Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)}").absolutePath

fun getFilenameFromNetworkPath(filePath: String) = URLUtil.guessFileName(filePath, null, null)

fun Activity.launchInstaller(filePath: String) {
    val file = File("${getDownloadDirectory()}/${getFilenameFromNetworkPath(filePath)}")
    var fileUri = file.toUri()
    if (Build.VERSION.SDK_INT >= 24) {
        fileUri = FileProvider.getUriForFile(this, "$packageName.fileprovider", file)
    }
    Log.d("util", "fileuri: $fileUri")
    startActivity(Intent(Intent.ACTION_VIEW, fileUri).apply {
        putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true)
        setDataAndType(fileUri, "application/vnd.android.package-archive")
        flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
    })
}

fun checkIfPresentInDownloadsFolder(filePath: String) = File(
    "${Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)}/${URLUtil.guessFileName(
        filePath,
        null,
        null
    )}"
).exists()

fun WebView.runJavascriptCompat(script: String, onResult: (String) -> Unit = {}) {
    Log.d("Utils", "running javascript command: $script")
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        this.evaluateJavascript(script) {
            Log.d("Utils", "result: $it")
            onResult.invoke(it)
        }
    } else {
        this.loadUrl("javascript:$script")
    }
}

fun Activity.isSkypeClientInstalled(): Boolean {
    try {
        packageManager.getPackageInfo("com.skype.raider", PackageManager.GET_ACTIVITIES)
    } catch (e: PackageManager.NameNotFoundException) {
        try {
            packageManager.getPackageInfo("com.skype.m2", PackageManager.GET_ACTIVITIES)
            return true
        } catch (f: PackageManager.NameNotFoundException) {
            return false
        }
    }
    return true
}

fun Activity.goToSkypeInPlaystore() {
    launchSkype("market://details?id=com.skype.raider")
}

fun Activity.launchSkype(url: String) {
    startActivity(Intent(Intent.ACTION_VIEW, url.toUri()).apply {
        setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
    })
}

fun cancelDownloads() {
    PRDownloader.cancelAll()
}

fun downloadFile(
    url: String,
    dirPath: String,
    filename: String,
    onStart: () -> Unit = {},
    onPause: () -> Unit = {},
    onCancel: () -> Unit = {},
    onProgress: (Int) -> Unit = {},
    onComplete: () -> Unit = {},
    onError: () -> Unit = {}
) =
    PRDownloader.download(url, dirPath, filename).build().setOnStartOrResumeListener {
        onStart.invoke()
    }.setOnPauseListener {
        onPause.invoke()
    }.setOnCancelListener {
        onCancel.invoke()
    }.setOnProgressListener {
        onProgress.invoke((((it.currentBytes.toFloat() / it.totalBytes) * 100).toInt()).also {
            Log.d("download", "progress: $it")
        })
    }.start(object : OnDownloadListener {
        override fun onDownloadComplete() {
            Log.d("download", "download complete")
            onComplete.invoke()
        }

        override fun onError(error: Error?) {
            Log.d("download", "download error")
            onError.invoke()
        }
    })

fun String?.getIfNotNullOrEmpty(): String? {
    if (this != null && this.isNotEmpty()) {
        return this
    }
    return null
}

fun Context.cacheFile(filename: String): File {
    val directory: File
    if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) {
        directory = Environment.getExternalStoragePublicDirectory(BuildConfig.APPLICATION_ID)
    } else {
        directory = this.filesDir
    }
    return File(directory, filename)
}

fun String.endsWithAny(vararg strings: String) = strings.filter { this.endsWith(it, true) }.size > 0


fun String.toHtml(): Spanned {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
        Html.fromHtml(this, Html.FROM_HTML_MODE_COMPACT)
    else
        Html.fromHtml(this)
}

fun Activity.browseExternal(url: String, newTask: Boolean = false): Boolean {
    try {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        if (newTask) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        this.startActivityForResult(intent, MainActivity.REQUEST_CODE_EXTERNAL_WEBVIEW)
        return true
    } catch (e: ActivityNotFoundException) {
        e.printStackTrace()
        return false
    }
}

const val NOTIF_CHANNEL_ID = "push_notification_001"
fun Context.showPushNotification(pushNotif: PushNotif, pushId: String, pushModuleAdapter: PushModuleAdapter) {
    val notificationManager = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    val builder = NotificationCompat.Builder(this, NOTIF_CHANNEL_ID)

    val deeplinkUrl = pushNotif.deepLink ?: "app://w88lite.com"
    val notifNum = pushId.generateNotificationID()

    val intent = Intent(Intent.ACTION_VIEW, deeplinkUrl.toUri()).apply {
        putExtra("title", pushNotif.title)
        putExtra("subject", pushNotif.subject)
        putExtra("message", pushNotif.message)
        putExtra("url", deeplinkUrl)
        putExtra("showAsPopup", pushNotif.showAsPopup)
        putExtra("uuid", pushId)
    }
    val startIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

//    builder.apply {
//        setContentIntent(startIntent)
//        setAutoCancel(true)
//        setSubText(pushNotif.subject)
//        setContentTitle(pushNotif.title)
//        setContentText(pushNotif.message)
//        setTicker("Push Notification")
//        setLargeIcon(BitmapFactory.decodeResource(this@showPushNotification.resources, R.mipmap.ic_launcher))
//        setSmallIcon(R.drawable.ic_logo_notif)
//        setColor(ContextCompat.getColor(this@showPushNotification, android.R.color.black))
//        setGroup(pushId)
//        setDeleteIntent(
//            PendingIntent.getService(
//                this@showPushNotification,
//                notifNum,
//                pushModuleAdapter.getRegisterNotificationIntent(this@showPushNotification, pushId, "dismissed"),
//                PendingIntent.FLAG_ONE_SHOT
//            )
//        )
//        pushNotif.vibrate.whenNotNullOrIsTrue { setVibrate(kotlin.longArrayOf(1000, 1000, 1000)) }
//        pushNotif.sounds.whenNotNullOrIsTrue { setSound(Settings.System.DEFAULT_NOTIFICATION_URI) }
//        pushNotif.lights.whenNotNullOrIsTrue { setLights(Color.WHITE, 300, 100) }
//    }
    val notificationId = pushId.hashCode()
    val notification = builder.build()
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val notificationChannel =
            NotificationChannel(NOTIF_CHANNEL_ID, NOTIF_CHANNEL_ID, NotificationManager.IMPORTANCE_DEFAULT)
        notificationManager.createNotificationChannel(notificationChannel)
        builder.setChannelId(NOTIF_CHANNEL_ID)
    }
    notificationManager.notify(pushId, notificationId, notification)
    if (pushNotif.endAt != 0L) {
        pushModuleAdapter.setNotificationDismissalSchedule(
            this@showPushNotification,
            pushNotif.endAt,
            pushId,
            notificationId
        )
    }
}