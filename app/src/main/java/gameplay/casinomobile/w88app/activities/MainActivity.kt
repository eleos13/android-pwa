package gameplay.casinomobile.w88app.activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DownloadManager
import android.app.KeyguardManager
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.util.Log
import android.view.View
import android.webkit.*
import androidx.annotation.RequiresApi
import androidx.core.net.toUri
import com.afollestad.materialdialogs.MaterialDialog
import com.cloudflare.api.CFMobile
import com.crashlytics.android.Crashlytics
import com.facebook.device.yearclass.YearClass
import com.google.gson.Gson
import gameplay.casinomobile.pwa.WebviewResourceMappingHelper
import gameplay.casinomobile.w88app.*
import gameplay.casinomobile.w88app.adapters.PushModuleAdapter
import gameplay.casinomobile.w88app.data.ContainerArgs
import gameplay.casinomobile.w88app.data.local.ConfigDataManager
import gameplay.casinomobile.w88app.data.local.UserAppState
import gameplay.casinomobile.w88app.data.models.ContainerInfo
import gameplay.casinomobile.w88app.data.models.PrecacheManifest
import gameplay.casinomobile.w88app.data.remote.NativeService
import gameplay.casinomobile.w88app.fragments.ContainerFragment
import gameplay.casinomobile.w88app.interfaces.Containable
import gameplay.casinomobile.w88app.interfaces.PWAJSBindingInterface
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.browse
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast
import org.koin.android.ext.android.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class MainActivity : BaseActivity(), Containable {

    companion object {
        val TAG = MainActivity::class.java.simpleName
        val REQUEST_CODE_GAME = 5456
        val REQUEST_CODE_WEBACTIVITY = 4546
        val BIOMETRIC_REQUEST_CODE = 1364
        val REQUEST_CODE_EXTERNAL_WEBVIEW = 7563
        private const val INTERCEPTOR_TAG = "interceptor"
    }

    private var dashboardUrl = BuildConfig.defaultDomain.buildDashboardUrl()
    private var containerMap: HashMap<String, ContainerFragment> = hashMapOf()
    private var isContainerVisible = false
    private var currentFragment: ContainerFragment? = null
    private var gameContainerOpened = false
    private var precacheUrl = BuildConfig.precacheOverride.getIfNotNullOrEmpty()
        ?: BuildConfig.precacheDomain.buildPrecacheManifestUrl()
    private var timeoutBeforeRequiringBio = BuildConfig.TIMEOUT_BEFORE_REQUIRE_BIO
    private var preloadMaxRetry: Int = BuildConfig.PRELOAD_MAX_RETRY
    private var precacheBackUrlIndex = 0
    private val deviceYearClass by lazy {
        YearClass.get(applicationContext)
    }
    val progressDialog by lazy {
        ProgressDialog(this@MainActivity).apply {
            setMessage(getString(R.string.downloading))
            setCancelable(false)
            setCanceledOnTouchOutside(false)
            setProgressStyle(ProgressDialog.STYLE_HORIZONTAL)
            max = 100
            progress = 0
        }
    }
    private val nativeService: NativeService by inject()
    private val configDataManager: ConfigDataManager by inject()
    private val pushAdapter: PushModuleAdapter by inject()

    @SuppressLint("AddJavascriptInterface")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "initializing fabric")
        Fabric.with(
            Fabric.Builder(this)
                .kits(Crashlytics())
                .appIdentifier(BuildConfig.APPLICATION_ID)
                .build()
        )
        setContentView(R.layout.activity_main)
        CFMobile.initialize(applicationContext, BuildConfig.CLOUDFLARE_API_KEY)
        requestPermission(
            permissions = *arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
            onGranted = {
                fetchDomainFromManifest()
                initPushy()
            },
            onDenied = { this@MainActivity.finish() })
        gameContainerOpened = false
        Log.d(TAG, "device year class: ${YearClass.get(applicationContext)}")
    }

    private fun initPushy() {
        Log.d(TAG, "initializing pushy.")
        pushAdapter.startListening(this@MainActivity)
        pushAdapter.subscribeNoLogin(this@MainActivity)
    }

    override fun onResume() {
        super.onResume()
        gameContainerOpened = false

        // call webview.isLoggedIn()

        runOnUiThread {
            wv_dashboard.runJavascriptCompat("window.isLoggedIn()", {
                if (it.equals("true") || it.toBoolean() || it.equals("1")) {
                    if ((UserAppState.exitAppTimestamp != 0L).also { Log.d(TAG, "timestamp not 0L") } && (System.currentTimeMillis() - UserAppState.exitAppTimestamp).also { Log.d(TAG, "current time minus exitapptimestamp = $it") } > this@MainActivity.timeoutBeforeRequiringBio) {
                        val km = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
                        if (km.isKeyguardSecure && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            startActivityForResult(
                                km.createConfirmDeviceCredentialIntent(
                                    getString(R.string.app_name),
                                    getString(R.string.auth_required)
                                ), BIOMETRIC_REQUEST_CODE
                            )
                        }
                    }
                }
            })
        }
    }

    override fun onPause() {
        super.onPause()
        progressDialog.cancel()
        cancelDownloads()
        WebviewResourceMappingHelper.getInstance().startDownload(this@MainActivity)
        runOnUiThread {
            wv_dashboard.runJavascriptCompat("window.isLoggedIn()") {
                if (it.equals("true") || it.toBoolean() || it.equals("1")) {
                    UserAppState.exitAppTimestamp = System.currentTimeMillis().also {
                        Log.d(TAG, "setting exitAppTimestamp to $it")
                    }
                }
            }
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        Log.d(TAG, "config change handled manually")
    }

    private fun fetchDomainFromManifest() {
        nativeService.fetchManifest(precacheUrl).enqueue(object : Callback<PrecacheManifest> {
            override fun onFailure(call: Call<PrecacheManifest>, t: Throwable) {
                fetchDomainFromManifestBackup()
            }

            override fun onResponse(call: Call<PrecacheManifest>, response: Response<PrecacheManifest>) {
                if (response.isSuccessful) {
                    Log.d(TAG, "$precacheUrl request succeeded. Parsing response")
                    precacheUrl = BuildConfig.precacheOverride.getIfNotNullOrEmpty()
                        ?: BuildConfig.precacheDomain.buildPrecacheManifestUrl()
                    precacheBackUrlIndex = 0
                    checkVersion(response.body()?.injectConfig)
                } else {
                    fetchDomainFromManifestBackup()
                }
            }
        })
    }

    private fun fetchDomainFromManifestBackup() {
        if (precacheBackUrlIndex < BuildConfig.precacheBackupDomains.size) {
            Log.d(
                TAG,
                "$precacheUrl failed, attempting to use ${BuildConfig.precacheBackupDomains[precacheBackUrlIndex].buildPrecacheManifestUrl()}"
            )
            precacheUrl = BuildConfig.precacheBackupDomains[precacheBackUrlIndex].buildPrecacheManifestUrl()
            precacheBackUrlIndex++
            fetchDomainFromManifest()
        } else {
            Log.d(TAG, "$precacheUrl failed, using default data")
            initWebview()
        }
    }

    private fun checkVersion(precacheManifest: PrecacheManifest.InjectConfig?) {
        if (BuildConfig.VERSION_CODE >= precacheManifest?.jsonVersion?.version ?: -1) {
            updateAppConstants(precacheManifest)
            initWebview()
        } else {
            MaterialDialog(this@MainActivity)
                .cancelable(false).show {
                    title(text = getString(R.string.dialog_update_title))
                    message(
                        text = getString(
                            R.string.dialog_update_message,
                            BuildConfig.VERSION_NAME,
                            precacheManifest?.jsonVersion?.versionName
                        ).toHtml(), html = true
                    )
                    val jsonVersion = precacheManifest?.jsonVersion!!
                    if (checkIfPresentInDownloadsFolder(jsonVersion.file!!)) {
                        positiveButton(text = getString(R.string.dialog_button_upgrade)) {
                            launchInstaller(jsonVersion.file)
                            finish()
                        }
                    } else {
                        positiveButton(text = getString(R.string.dialog_button_download)) {
                            startDownload(jsonVersion.file, progressDialog)
                        }
                    }
                    negativeButton(text = getString(R.string.dialog_button_cancel)) {
                        if (jsonVersion.required!!) {
                            this@MainActivity.finish()
                        } else {
                            updateAppConstants(precacheManifest)
                            initWebview()
                        }
                    }
                }
        }
    }

    private fun startDownload(file: String, progressDialog: ProgressDialog) {
        downloadFile(
            url = file,
            dirPath = getDownloadDirectory(),
            filename = getFilenameFromNetworkPath(file),
            onStart = { progressDialog.show() },
            onProgress = { progressDialog.progress = it },
            onComplete = {
                progressDialog.dismiss()
                launchInstaller(file)
                finish()
            },
            onError = {
                progressDialog.dismiss()
                MaterialDialog(this@MainActivity).show {
                    message(text = "An error occured.")
                    positiveButton(text = "Retry") {
                        // initiate download method
                        it.dismiss()
                        startDownload(file, progressDialog)
                    }
                }
            }
        )
    }

    private fun updateAppConstants(precacheManifest: PrecacheManifest.InjectConfig?) {
        if (precacheManifest != null) {
            precacheManifest.apply {
                dashboardUrl = when (Locale.getDefault().language.toLowerCase().substring(0, 2)) {
                    "zh" -> memberSiteDomainZhCn ?: memberSiteDomain
                    "en" -> memberSiteDomainEnUs ?: memberSiteDomain
                    "id" -> memberSiteDomainIdId ?: memberSiteDomain
                    "ja" -> memberSiteDomainJaJp ?: memberSiteDomain
                    "km" -> membersiteDomainKmKh ?: memberSiteDomain
                    "ko" -> membersiteDomainKoKr ?: memberSiteDomain
                    "th" -> membersiteDomainThTh ?: memberSiteDomain
                    "vi" -> membersiteDomainViVn ?: memberSiteDomain
                    else -> memberSiteDomain
                }.buildDashboardUrl()
                this@MainActivity.timeoutBeforeRequiringBio = timeoutBeforeRequiringBio ?: BuildConfig.TIMEOUT_BEFORE_REQUIRE_BIO
                this@MainActivity.preloadMaxRetry = preloadMaxRetry ?: BuildConfig.PRELOAD_MAX_RETRY
            }
        }
    }

    @SuppressLint("AddJavascriptInterface", "SetJavaScriptEnabled")
    private fun initWebview() {

        CookieManager.getInstance().setAcceptCookie(true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(wv_dashboard, true)
        }

        wv_dashboard.apply {
            setBackgroundColor(Color.TRANSPARENT)
            settings.apply {
                javaScriptEnabled = true
                pluginState = WebSettings.PluginState.ON
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    mediaPlaybackRequiresUserGesture = false
                }
                javaScriptCanOpenWindowsAutomatically = false
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
                }
                builtInZoomControls = false
                loadWithOverviewMode = true
                setRenderPriority(WebSettings.RenderPriority.HIGH)
                useWideViewPort = true
                setSupportMultipleWindows(false)
                setSupportZoom(false)
                domStorageEnabled = true
                allowFileAccess = true
                allowFileAccessFromFileURLs = true
                allowUniversalAccessFromFileURLs = true
                setAppCacheEnabled(true)
                cacheMode = WebSettings.LOAD_NO_CACHE
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    setRendererPriorityPolicy(WebView.RENDERER_PRIORITY_IMPORTANT, false)
                }
            }
            webViewClient = object : WebViewClient() {
                override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                    super.onPageStarted(view, url, favicon)
                    Log.d(TAG, "onpagestarted, url: $url")
                }

                @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest): Boolean {
                    if (request.url.toString().startsWith(
                            "tel:",
                            "sms:",
                            "smsto:",
                            "mms:",
                            "mmsto:",
                            "mailto:"
                        )
                    ) {
                        this@MainActivity.startActivity(Intent(Intent.ACTION_VIEW, request.url.toString().toUri()))
                        return true
                    } else if (request.url.toString().startsWith("skype:")) {
                        if (isSkypeClientInstalled()) {
                            launchSkype(request.url.toString())
                        } else {
                            goToSkypeInPlaystore()
                        }
                        return true
                    }
                    return false
                }

                override fun shouldInterceptRequest(
                    view: WebView?,
                    request: WebResourceRequest?
                ): WebResourceResponse? {
                    var webResourceResponse: WebResourceResponse? = null
                    view?.post {
                        try {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                val filename = request?.url.toString().split("/").last()
                                val fileExt = WebviewResourceMappingHelper.getInstance().getFileExt(filename)
                                if (WebviewResourceMappingHelper.getInstance().overridableExtensions.contains(fileExt)) {
                                    val encoding = "UTF-8"
                                    val mimeType = WebviewResourceMappingHelper.getInstance().getMimeType(fileExt)
                                    try {
                                        // first we try to load from assets folder
                                        webResourceResponse =
                                            WebviewResourceMappingHelper.getWebResourceResponseFromAsset(
                                                this@MainActivity,
                                                filename,
                                                mimeType,
                                                encoding
                                            )
                                    } catch (e: Exception) {
                                        webResourceResponse = null
                                    }
                                    if (webResourceResponse == null) {
                                        // still null, we check from local storage
                                        val file = cacheFile(filename)
                                        if (file.exists()) {
                                            webResourceResponse =
                                                WebviewResourceMappingHelper.getWebResourceResponseFromFile(
                                                    file,
                                                    mimeType,
                                                    encoding
                                                )
                                        } else {
                                            webResourceResponse = null
                                        }
                                    }
                                    if (webResourceResponse == null) {
                                        // still null, we load from web
                                        if (request?.url.toString().endsWithAny("jpg", "jpeg", "png", "ico")) {
                                            WebviewResourceMappingHelper.getInstance().queueForDownload(
                                                request?.url.toString()
//                                                    .replace(dashboardUrl,
//                                                    "https://m.media884.net/"
//                                                )
                                            )
                                        }
                                    }
                                }
                            }
                        } catch (e: Exception) {
                        }
                    }
                    if (webResourceResponse == null) {
                        return super.shouldInterceptRequest(view, request)
                    } else {
                        return webResourceResponse
                    }
                }

                override fun shouldOverrideUrlLoading(view: WebView?, url: String): Boolean {
                    if (url.startsWith("tel:", "sms:", "smsto:", "mms:", "mmsto:", "mailto:")) {
                        this@MainActivity.startActivity(Intent(Intent.ACTION_VIEW, url.toUri()))
                        return true
                    } else if (url.startsWith("skype:")) {
                        if (isSkypeClientInstalled()) {
                            launchSkype(url)
                        } else {
                            goToSkypeInPlaystore()
                        }
                        return true
                    }
                    return false
                }
            }
            addJavascriptInterface(object : PWAJSBindingInterface() {
                @JavascriptInterface
                override fun platform() = BuildConfig.VERSION_NAME

                @JavascriptInterface
                override fun openExternalBrowser(url: String) {
                    Log.d(TAG, "url: $url")
                    browse(url)
                }

                @JavascriptInterface
                override fun clearEverything() {
//                    deleteCookies()
//                    WebStorage.getInstance().deleteAllData()
                    runOnUiThread { WebView(this@MainActivity).clearCache(true) }
                }

                @JavascriptInterface
                override fun shouldPreloadGames() = deviceYearClass >= 2014

                @JavascriptInterface
                override fun showDashboard() {
                    if (this@MainActivity.isFinishing.not()) {
                        this@MainActivity.showDashboard()
                    }
                }

                @JavascriptInterface
                override fun preloadContainers(stringifiedContainerObjects: String, showFab: Boolean) {
                    if (this@MainActivity.isFinishing.not()) {
                        Gson().fromJson(stringifiedContainerObjects, Array<ContainerInfo>::class.java).forEach {
                            runOnUiThread {
                                this@MainActivity.preloadContainer(
                                    containerId = it.containerId,
                                    fullPath = it.fullPath,
                                    showFab = showFab
                                )
                            }
                        }
                    }
                }

                @JavascriptInterface
                override fun preloadContainers(stringifiedContainerObjects: String) {
                    if (this@MainActivity.isFinishing.not()) {
                        Gson().fromJson(stringifiedContainerObjects, Array<ContainerInfo>::class.java).forEach {
                            runOnUiThread {
                                this@MainActivity.preloadContainer(
                                    containerId = it.containerId,
                                    fullPath = it.fullPath,
                                    showFab = false
                                )
                            }
                        }
                    }
                }

                @JavascriptInterface
                override fun loadContainer(containerId: String) {
                    runOnUiThread {
                        this@MainActivity.loadContainer(containerId)
                    }
                }

                @JavascriptInterface
                override fun openInContainer(
                    link: String,
                    containerId: String,
                    showFab: Boolean,
                    forceDashboard: Boolean?,
                    removeNativeBinding: Boolean?
                ) {
                    Log.d(TAG, "open in container called.")
                    runOnUiThread {
                        if (link.isNotEmpty()) {
                            Log.d(TAG, "open in container called. url: $link")
                            this@MainActivity.openInContainer(
                                link,
                                containerId,
                                showFab,
                                forceDashboard ?: false,
                                removeNativeBinding ?: false
                            )
                        } else {
                            Log.d(TAG, "open in container called, url is empty.")
                        }
                    }
                }

                @JavascriptInterface
                override fun setItem(key: String, data: String) {
                    Log.d(TAG, "config data: set item $key - $data")
                    runOnUiThread {
                        configDataManager.setItem(key, data)
                    }
                }

                @JavascriptInterface
                override fun getItem(key: String): String? {
                    return configDataManager.getItem(key).also {
                        Log.d(TAG, "config data: get item $key - $it")
                    }
                }

            }, "Native")
            Log.d(TAG, "loading $dashboardUrl for dashboard")
            loadUrl(dashboardUrl, mapOf("Cache-Control" to "no-cache"))
            setDownloadListener { url, userAgent, contentDisposition, mimetype, contentLength ->
                val request = DownloadManager.Request(Uri.parse(url)).apply {
                    setMimeType(mimetype)
                }
                val cookies = CookieManager.getInstance().getCookie(url)
                request.apply {
                    addRequestHeader("cookie", cookies)
                    addRequestHeader("User-Agent", userAgent)
                    setDescription("Download file...")
                    setTitle(URLUtil.guessFileName(url, contentDisposition, mimetype))
                    allowScanningByMediaScanner()
                    setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                    setDestinationInExternalPublicDir(
                        Environment.DIRECTORY_DOWNLOADS,
                        URLUtil.guessFileName(url, contentDisposition, mimetype)
                    )
                }
                val dm = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
                dm.enqueue(request)
                toast("Downloading file")
            }
        }
    }

    override fun onBackPressed() {
        runOnUiThread {
            if (isContainerVisible) {
                wv_dashboard.runJavascriptCompat("window.fetchWallets()") {
                    Log.d(TAG, "result of fetchWallets: $it")
                }
                supportFragmentManager.beginTransaction().hide(currentFragment!!).commitAllowingStateLoss()
                currentFragment!!.onContainerHidden()
                isContainerVisible = false
            } else {
                wv_dashboard.runJavascriptCompat("backButtonPressed()") {
                    if (it.equals("true") || it.equals("1") || it.equals("\"true\"")) {
                        Log.d(TAG, "javascript back is consumed, not handled by native.")
                    } else {
                        if (wv_dashboard.canGoBack()) {
                            wv_dashboard.goBack()
                        } else {
                            showNativeExitDialog()
                        }
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(TAG, "requestcode: $requestCode resultCode: $resultCode")
        if (requestCode == BIOMETRIC_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                UserAppState.exitAppTimestamp = 0
            } else {
                // biometric failed, call logout on webview
                runOnUiThread {
                    wv_dashboard.runJavascriptCompat("window.logout()")
                }
            }
        } else if (requestCode == REQUEST_CODE_GAME || requestCode == REQUEST_CODE_WEBACTIVITY) {
            UserAppState.exitAppTimestamp = 0
            runOnUiThread {
                wv_dashboard.runJavascriptCompat("window.fetchWallets()")
            }
            if (resultCode == Activity.RESULT_OK) {
                runOnUiThread {
                    wv_dashboard.runJavascriptCompat("window.redirectTo('${data?.getStringExtra(GameActivity.KEY_REDIRECT_COMMAND)}');")
                }
            }
        }
    }

    override fun closeContainerAndRedirect(command: String) {
        onBackPressed()
        evaluateCommandJS(command)
    }

    override fun executeCommand(command: String) {
        evaluateCommandJS(command)
    }

    private fun evaluateCommandJS(command: String) {
        Log.d(TAG, "command: $command")
        runOnUiThread {
            wv_dashboard.runJavascriptCompat(command)
        }
    }

    override fun onContainerReady(containerId: String?) {
        if (!containerId.isNullOrEmpty()) {
            callReady(containerId)
        }
    }

    private fun openInContainer(
        url: String,
        containerId: String,
        showFab: Boolean,
        forceDashboard: Boolean,
        shouldRemoveNativeBinding: Boolean
    ) {
        if (gameContainerOpened.not()) {
            gameContainerOpened = true
            // start game activity
            startActivityForResult(
                intentFor<GameActivity>(
                    GameActivity.KEY_URL to url,
                    GameActivity.KEY_SHOW_FAB to showFab,
                    GameActivity.KEY_FORCE_DASHBOARD to forceDashboard,
                    GameActivity.KEY_REMOVE_NATIVEBINDING to shouldRemoveNativeBinding
                ), REQUEST_CODE_GAME
            )
        }
    }

    private fun loadContainer(containerId: String) {
        if (containerMap.containsKey(containerId)) {
            val newCurrentFragment = supportFragmentManager.findFragmentByTag(containerId)!! as ContainerFragment
            if (currentFragment != null) {
                supportFragmentManager.beginTransaction().hide(currentFragment!!)
                    .show(newCurrentFragment).commitAllowingStateLoss()
            } else {
                supportFragmentManager.beginTransaction().show(newCurrentFragment).commitAllowingStateLoss()
            }
            currentFragment = newCurrentFragment
            isContainerVisible = true
        }
    }

    private fun showDashboard() {
        if (isContainerVisible) {
            onBackPressed()
        }
    }

    private fun preloadContainer(containerId: String, fullPath: String, showFab: Boolean) {
        if (containerMap.containsKey(containerId).not()) {
            val containerFragment = newFragment<ContainerArgs, ContainerFragment>(ContainerArgs(fullPath, showFab, containerId, preloadMaxRetry))
            containerMap.put(containerId, containerFragment)
            supportFragmentManager.beginTransaction().add(R.id.fl_fragment_container, containerFragment, containerId)
                .hide(containerFragment).commitAllowingStateLoss()
        } else {
            containerMap.get(containerId)?.reinitWithUrl(fullPath)
        }
    }

    private fun callReady(containerId: String) {
        Log.d("ContainerFragment", "container ready: $containerId")
        Handler().postDelayed({
            runOnUiThread {
                wv_dashboard.runJavascriptCompat("window.containerReady('$containerId')")
            }
        }, 100)
    }

    private fun showNativeExitDialog() {
        MaterialDialog(this).show {
            message(R.string.dialog_content_close_application)
            positiveButton(R.string.dialog_ok) {
                finish()
            }
            negativeButton(R.string.dialog_button_cancel) {
                it.dismiss()
            }
        }
    }

    private fun deleteCookies() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            CookieManager.getInstance().removeAllCookies(null)
        } else {
            CookieSyncManager.createInstance(application)?.apply {
                startSync()
                CookieManager.getInstance()?.apply {
                    removeAllCookie()
                    setAcceptCookie(true)
                }
                stopSync()
            }
        }
    }

    override fun connectionLost() {
        try {
            wv_dashboard.setBackgroundColor(Color.TRANSPARENT)
        } catch (e: Exception) {
        }
    }

    override fun connectionRestored() {
        try {
            wv_dashboard.setBackgroundColor(Color.WHITE)
        } catch (e: Exception) {
        }
    }

    override fun getParentView() = parentPanel
}