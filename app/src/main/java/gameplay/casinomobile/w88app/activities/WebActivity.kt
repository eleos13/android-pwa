package gameplay.casinomobile.w88app.activities

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import android.webkit.CookieManager
import android.webkit.CookieSyncManager
import android.webkit.JavascriptInterface
import com.afollestad.materialdialogs.MaterialDialog
import gameplay.casinomobile.w88app.R
import gameplay.casinomobile.w88app.interfaces.WebJSBindingInterface
import kotlinx.android.synthetic.main.activity_web.*

class WebActivity : BaseActivity() {
    override fun getParentView() = parentPanel

    override fun connectionLost() {
        try {
            web.setBackgroundColor(Color.TRANSPARENT)
        } catch (e: Exception) {
        }
    }

    override fun connectionRestored() {
        try {
            web.setBackgroundColor(Color.TRANSPARENT)
        } catch (e: Exception) {
        }
    }

    companion object {
        val TAG = WebActivity::class.java.simpleName
        val EXTRA_URL = "url"
        val EXTRA_APPBAR_VISIBILITY = "appbar_visibility"
        val EXTRA_CLEAR_COOKIES = "clear_cookies"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)

        setSupportActionBar(toolbar)
        supportActionBar?.title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        appbar.visibility = if (intent.getBooleanExtra(EXTRA_APPBAR_VISIBILITY, true)) View.VISIBLE else View.GONE

        loadPage()
    }

    @SuppressLint("SetJavaScriptEnabled", "AddJavascriptInterface")
    private fun loadPage() {
        web.apply {
            setBackgroundColor(Color.TRANSPARENT)
            settings.apply {
                builtInZoomControls = true
                setSupportZoom(true)
                javaScriptEnabled = true
            }
            addJavascriptInterface(object : WebJSBindingInterface() {
                @JavascriptInterface
                override fun onBackNativeHomePage() {
                    this@WebActivity.finish()
                }

                @JavascriptInterface
                override fun onBackPressed() {
                    this@WebActivity.finish()
                }

                @JavascriptInterface
                override fun showPopup(text: String) {
                    MaterialDialog(this@WebActivity).show {
                        message(text = text)
                        positiveButton(R.string.dialog_ok) {
                            this@WebActivity.finish()
                        }
                    }
                }
            }, "Native")
            if (intent?.extras?.getBoolean(EXTRA_CLEAR_COOKIES, false) == true) {
                clearCookies()
            }
            if (intent?.extras?.containsKey(EXTRA_URL) == true) {
                loadUrl(intent.extras.getString(EXTRA_URL))
            }
        }
    }

    private fun clearCookies() {
        web.clearCache(true)
        web.clearHistory()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            CookieManager.getInstance().removeAllCookies(null)
            CookieManager.getInstance().flush()
        } else {
            val cookieSyncManager = CookieSyncManager.createInstance(this@WebActivity)
            cookieSyncManager.startSync()
            val cookieManager = CookieManager.getInstance()
            cookieManager.removeAllCookie()
            cookieManager.removeSessionCookie()
            cookieSyncManager.stopSync()
            cookieSyncManager.sync()
        }

    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}
