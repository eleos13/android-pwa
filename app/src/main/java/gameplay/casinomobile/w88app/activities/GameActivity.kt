package gameplay.casinomobile.w88app.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DownloadManager
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.net.http.SslError
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.webkit.*
import android.widget.ImageView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import com.afollestad.materialdialogs.MaterialDialog
import gameplay.casinomobile.w88app.*
import gameplay.casinomobile.w88app.customviews.FloatingActionMenu
import gameplay.casinomobile.w88app.customviews.SubActionButton
import gameplay.casinomobile.w88app.interfaces.MobileJSBindingInterface
import kotlinx.android.synthetic.main.activity_game.*
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.toast

class GameActivity : BaseActivity() {
    override fun getParentView() = parentPanel

    override fun connectionLost() {
        try {
            Log.d(TAG, "connection lost")
            wv_game?.setBackgroundColor(Color.WHITE)
        } catch (e: Exception) {
        }
    }

    override fun connectionRestored() {
        try {
            Log.d(TAG, "connection resumed")
            wv_game?.setBackgroundColor(Color.TRANSPARENT)
        } catch (e: Exception) {
        }
    }

    companion object {
        val TAG = GameActivity::class.java.simpleName
        val KEY_URL = "defaultUrl"
        val KEY_SHOW_FAB = "showFab"
        val KEY_FORCE_DASHBOARD = "forceDashboard"
        val KEY_REMOVE_NATIVEBINDING = "removeNativeBinding"
        val KEY_REDIRECT_COMMAND = "redirectCommand"
    }

    var forceDashboard: Boolean = false
    lateinit var floatingActionMenu: FloatingActionMenu

    @SuppressLint("SetJavaScriptEnabled", "AddJavascriptInterface")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        val url = intent.getStringExtra(KEY_URL)
        val showFab = intent.getBooleanExtra(KEY_SHOW_FAB, true)
        forceDashboard = intent.getBooleanExtra(KEY_FORCE_DASHBOARD, false)
        val removeNativeBinding = intent.getBooleanExtra(KEY_REMOVE_NATIVEBINDING, false)

        layout_menu?.also {
            if (showFab) {
                it.visibility = View.VISIBLE
                setupFab()
            } else {
                it.visibility = View.GONE
            }
        }

        wv_game?.apply {

            CookieManager.getInstance().setAcceptCookie(true)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                CookieManager.getInstance().setAcceptThirdPartyCookies(this, true)
            }

            setBackgroundColor(Color.TRANSPARENT)
            settings.apply {
                javaScriptEnabled = true
                pluginState = WebSettings.PluginState.ON
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    mediaPlaybackRequiresUserGesture = false
                }
                javaScriptCanOpenWindowsAutomatically = false
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
                }
                builtInZoomControls = false
                loadWithOverviewMode = true
                setRenderPriority(WebSettings.RenderPriority.HIGH)
                useWideViewPort = true
                setSupportMultipleWindows(false)
                setSupportZoom(false)
                domStorageEnabled = true
                allowFileAccess = true
                allowFileAccessFromFileURLs = true
                allowUniversalAccessFromFileURLs = true
                setAppCacheEnabled(true)
                cacheMode = WebSettings.LOAD_DEFAULT
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    setRendererPriorityPolicy(WebView.RENDERER_PRIORITY_IMPORTANT, false)
                }
            }
            webViewClient = object : WebViewClient() {
                override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                    super.onPageStarted(view, url, favicon)
                    Log.d(TAG, "url: $url")
                    if (showFab &&
                        url != null &&
                        url.isNotEmpty() &&
                        url.toUri().host!!.matches(BuildConfig.defaultDomain.toUri().host!!.toRegex()) &&
                        url.contains("Launcher", true).not()
                    ) {
                        this@GameActivity.setResult(Activity.RESULT_OK, Intent().apply {
                            putExtra(
                                KEY_REDIRECT_COMMAND,
                                url.replace("https?://${BuildConfig.defaultDomain.toUri().host}".toRegex(), "")
                            )
                            Log.d(TAG, "extra: ${this.getStringExtra(KEY_REDIRECT_COMMAND)}")
                        })
                        overridePendingTransition(0, 0)
                        finish()
                    }
                }

                @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest): Boolean {
                    if (request.url.toString().startsWith(
                            "tel:",
                            "sms:",
                            "smsto:",
                            "mms:",
                            "mmsto:",
                            "mailto:"
                        )
                    ) {
                        startActivity(Intent(Intent.ACTION_VIEW, request.url.toString().toUri()))
                        return true
                    } else if (request.url.toString().startsWith("skype:")) {
                        if (isSkypeClientInstalled()) {
                            launchSkype(request.url.toString())
                        } else {
                            goToSkypeInPlaystore()
                        }
                        return true
                    }
                    return false
                }

                override fun shouldOverrideUrlLoading(view: WebView?, url: String): Boolean {
                    if (url.startsWith("tel:", "sms:", "smsto:", "mms:", "mmsto:", "mailto:")) {
                        startActivity(Intent(Intent.ACTION_VIEW, url.toUri()))
                        return true
                    } else if (url.startsWith("skype:")) {
                        if (isSkypeClientInstalled()) {
                            launchSkype(url)
                        } else {
                            goToSkypeInPlaystore()
                        }
                        return true
                    }
                    return false
                }

                override fun onReceivedHttpError(
                    view: WebView?,
                    request: WebResourceRequest?,
                    errorResponse: WebResourceResponse?
                ) {
                    super.onReceivedHttpError(view, request, errorResponse)
                    wv_game?.setBackgroundColor(Color.WHITE)
                }

                override fun onReceivedSslError(view: WebView?, handler: SslErrorHandler?, error: SslError?) {
                    if (handler != null) {
                        handler.proceed()
                    } else {
                        super.onReceivedSslError(view, null, error)
                    }
                    wv_game?.setBackgroundColor(Color.WHITE)
                }

                override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
                    super.onReceivedError(view, request, error)
                    wv_game?.setBackgroundColor(Color.WHITE)
                }
            }
            if (removeNativeBinding.not()) {
                addJavascriptInterface(object : MobileJSBindingInterface() {

                    @JavascriptInterface
                    override fun nativeBack() {
                        finish()
                    }

                    @JavascriptInterface
                    override fun onSubmitDeposit(url: String) {
                    }

                    @JavascriptInterface
                    override fun openExternalBrowser(url: String) {
                    }

                    @JavascriptInterface
                    override fun openApp(packageId: String, downloadUrl: String) {
                    }

                    @JavascriptInterface
                    override fun onNativeBackPressed() {
                        finish()
                    }

                    @JavascriptInterface
                    override fun closeContainerAndRedirect(path: String) {
                        setResult(Activity.RESULT_OK, Intent().apply {
                            putExtra(KEY_REDIRECT_COMMAND, path)
                        })
                        overridePendingTransition(0, 0)
                        finish()
                    }

                    @JavascriptInterface
                    override fun executeCommand(command: String) {
                    }

                    @JavascriptInterface
                    override fun onSlotGameClosed() {
                    }
                }, "Native")
            }
            loadUrl(url)
            setDownloadListener { url, userAgent, contentDisposition, mimetype, contentLength ->
                val request = DownloadManager.Request(Uri.parse(url)).apply {
                    setMimeType(mimetype)
                }
                val cookies = CookieManager.getInstance().getCookie(url)
                request.apply {
                    addRequestHeader("cookie", cookies)
                    addRequestHeader("User-Agent", userAgent)
                    setDescription("Download file...")
                    setTitle(URLUtil.guessFileName(url, contentDisposition, mimetype))
                    allowScanningByMediaScanner()
                    setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                    setDestinationInExternalPublicDir(
                        Environment.DIRECTORY_DOWNLOADS,
                        URLUtil.guessFileName(url, contentDisposition, mimetype)
                    )
                }
                val dm = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
                dm.enqueue(request)
                toast("Downloading file")
                overridePendingTransition(0, 0)
                finish()
            }
        }
    }

    private fun setupFab() {
        val itemBuilder = SubActionButton.Builder(this@GameActivity)
        val homeButton = itemBuilder.setContentView(ImageView(this@GameActivity).apply {
            setImageDrawable(ContextCompat.getDrawable(this@GameActivity, R.drawable.ic_fab_home))
        }).build().apply {
            setOnClickListener {
                overridePendingTransition(0, 0)
                finish()
            }
        }
        val backButton = itemBuilder.setContentView(ImageView(this@GameActivity).apply {
            setImageDrawable(ContextCompat.getDrawable(this@GameActivity, R.drawable.ic_fab_back))
        }).build().apply {
            setOnClickListener {
                overridePendingTransition(0, 0)
                finish()
            }
        }
        val cashierButton = itemBuilder.setContentView(ImageView(this@GameActivity).apply {
            setImageDrawable(ContextCompat.getDrawable(this@GameActivity, R.drawable.ic_fab_cashier))
        }).build().apply {
            setOnClickListener {
                setResult(Activity.RESULT_OK, Intent().apply {
                    putExtra(KEY_REDIRECT_COMMAND, "/funds")
                })
                overridePendingTransition(0, 0)
                finish()
            }
        }

        floatingActionMenu = FloatingActionMenu.Builder(this@GameActivity).setStartAngle(248)
            .setEndAngle(112)
            .setRadius(46f.dpToPx())
            .addSubActionView(homeButton)
            .addSubActionView(backButton)
            .addSubActionView(cashierButton)
            .attachTo(img_arrow)
            .setStateChangeListener(object : FloatingActionMenu.MenuStateChangeListener {
                override fun onMenuOpened(menu: FloatingActionMenu?) {
                    img_glow.visibility = View.VISIBLE
                    layout_dim.visibility = View.VISIBLE
                }

                override fun onMenuClosed(menu: FloatingActionMenu?) {
                    img_glow.visibility = View.INVISIBLE
                    layout_dim.visibility = View.GONE
                }
            }).build()

        img_arrow.setOnTouchListener(object : View.OnTouchListener {
            private val CLICK_DRAG_TOLERANCE = 10f
            private var downRawX: Float = 0.toFloat()
            private var downRawY: Float = 0.toFloat()
            private var dX: Float = 0.toFloat()
            private var dY: Float = 0.toFloat()

            override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {
                if (img_glow.getVisibility() == View.VISIBLE) {
                    return false
                }

                val action = motionEvent.action
                if (action == MotionEvent.ACTION_DOWN) {

                    downRawX = motionEvent.rawX
                    downRawY = motionEvent.rawY
                    dX = view.x - downRawX
                    dY = view.y - downRawY

                    return true // Consumed
                } else if (action == MotionEvent.ACTION_MOVE) {

                    val viewWidth = view.width
                    val viewHeight = view.height

                    val viewParent = view.parent as View
                    val parentWidth = viewParent.width
                    val parentHeight = viewParent.height

                    var newX = motionEvent.rawX + dX
                    newX = Math.max(0f, newX) // Don't allow the FAB past the left hand side of the parent
                    newX = Math.min(
                        (parentWidth - viewWidth).toFloat(),
                        newX
                    ) // Don't allow the FAB past the right hand side of the parent

                    var newY = motionEvent.rawY + dY
                    newY = Math.max(0f, newY) // Don't allow the FAB past the top of the parent
                    newY = Math.min(
                        (parentHeight - viewHeight).toFloat(),
                        newY
                    ) // Don't allow the FAB past the bottom of the parent

                    img_glow.animate()
                        .y(newY).setDuration(0).start()

                    view.animate()
                        .y(newY).setDuration(0).start()

                    return true // Consumed
                } else if (action == MotionEvent.ACTION_UP) {

                    val upRawX = motionEvent.rawX
                    val upRawY = motionEvent.rawY

                    val upDX = upRawX - downRawX
                    val upDY = upRawY - downRawY

                    if (Math.abs(upDX) < CLICK_DRAG_TOLERANCE && Math.abs(upDY) < CLICK_DRAG_TOLERANCE) { // A click
                        view.performClick()
                        return true
                    } else { // A drag
                        return true // Consumed
                    }
                } else {
                    return onTouchEvent(motionEvent)
                }
            }
        })

        img_arrow.setOnClickListener {
            if (floatingActionMenu.isOpen) {
                floatingActionMenu.close(true)
            } else {
                floatingActionMenu.open(true)
            }
        }

        layout_dim.onClick { img_arrow.performClick() }
    }

    override fun onDestroy() {
        try {
            wv_game.destroy()
        } catch (e: Exception) {
        }
        super.onDestroy()
    }
}
