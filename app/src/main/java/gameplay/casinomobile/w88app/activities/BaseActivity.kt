package gameplay.casinomobile.w88app.activities

import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import android.os.Build
import android.view.View
import android.view.ViewTreeObserver
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import gameplay.casinomobile.w88app.R
import gameplay.casinomobile.w88app.interfaces.NetworkLostAware
import org.jetbrains.anko.connectivityManager
import org.jetbrains.anko.contentView


abstract class BaseActivity : AppCompatActivity(), NetworkLostAware {

    companion object {
        val TAG = BaseActivity::class.java.simpleName
    }

    private var layoutReady = false
    private val networkCallback: ConnectivityManager.NetworkCallback by lazy {
        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network?) {
                super.onAvailable(network)
                if (layoutReady) {
                    connectionRestored()
                    hideNoConnectionMessage()
                }
            }

            override fun onLost(network: Network?) {
                super.onLost(network)
                if (layoutReady) {
                    connectionLost()
                    showNoConnectionMessage()
                }
            }
        }
    }
    private val noInternetConnectionSnackbar by lazy {
        Snackbar.make(getParentView(), R.string.no_internet_connection, Snackbar.LENGTH_INDEFINITE).apply {
            view.setBackgroundColor(ContextCompat.getColor(this@BaseActivity, android.R.color.holo_red_light))
        }
    }

    override fun onResume() {
        super.onResume()
        contentView?.viewTreeObserver?.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                contentView?.viewTreeObserver?.removeGlobalOnLayoutListener(this)
                layoutReady = true
            }
        })

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            createNetworkChangeMonitor()
        }

        if(isNetworkConnected().not()) {
            showNoConnectionMessage()
        }
    }

    override fun onPause() {
        super.onPause()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            connectivityManager.unregisterNetworkCallback(networkCallback)
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun createNetworkChangeMonitor() {
        connectivityManager.registerNetworkCallback(NetworkRequest.Builder().build(), networkCallback)
    }

    private fun showNoConnectionMessage() {
        noInternetConnectionSnackbar.show()
    }

    private fun hideNoConnectionMessage() {
        noInternetConnectionSnackbar.dismiss()
    }

}