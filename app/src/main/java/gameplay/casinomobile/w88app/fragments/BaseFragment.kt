package gameplay.casinomobile.w88app.fragments

import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.view.ViewTreeObserver
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import gameplay.casinomobile.w88app.FRAGMENT_ARG
import gameplay.casinomobile.w88app.interfaces.NetworkLostAware
import org.jetbrains.anko.connectivityManager

abstract class BaseFragment<ARG : Parcelable> : Fragment(), NetworkLostAware {

    protected lateinit var arg: ARG
    private var layoutReady = false
    private val networkCallback: ConnectivityManager.NetworkCallback by lazy {
        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network?) {
                super.onAvailable(network)
                if (layoutReady) {
                    connectionRestored()
                }
            }

            override fun onLost(network: Network?) {
                super.onLost(network)
                if (layoutReady) {
                    connectionLost()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { arg = it.getParcelable(FRAGMENT_ARG)!! }
    }

    override fun onResume() {
        super.onResume()
        view?.viewTreeObserver?.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                view?.viewTreeObserver?.removeGlobalOnLayoutListener(this)
                layoutReady = true
            }
        })
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            createNetworkChangeMonitor()
        }
    }

    override fun onPause() {
        super.onPause()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity?.connectivityManager?.unregisterNetworkCallback(networkCallback)
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun createNetworkChangeMonitor() {
        activity?.connectivityManager?.registerNetworkCallback(NetworkRequest.Builder().build(), networkCallback)
    }
}
