package gameplay.casinomobile.w88app.fragments


import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.net.http.SslError
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import android.widget.ImageView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import gameplay.casinomobile.w88app.*
import gameplay.casinomobile.w88app.activities.MainActivity
import gameplay.casinomobile.w88app.activities.WebActivity
import gameplay.casinomobile.w88app.customviews.FloatingActionMenu
import gameplay.casinomobile.w88app.customviews.SubActionButton
import gameplay.casinomobile.w88app.data.ContainerArgs
import gameplay.casinomobile.w88app.data.models.PrecacheManifest
import gameplay.casinomobile.w88app.interfaces.Containable
import gameplay.casinomobile.w88app.interfaces.MobileJSBindingInterface
import kotlinx.android.synthetic.main.activity_web.view.*
import kotlinx.android.synthetic.main.fragment_container.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.support.v4.runOnUiThread

class ContainerFragment : BaseFragment<ContainerArgs>() {
    override fun getParentView() = parentPanel

    override fun connectionLost() {
        try {
            wv_container.setBackgroundColor(Color.WHITE)
            containerUrlChanged = true
        } catch (e: Exception) {
        }
    }

    override fun connectionRestored() {
        try {
            runOnUiThread {
                wv_container.loadUrl(containerUrl)
            }
            containerUrlChanged = true
        } catch (e: Exception) {
        }
    }

    private var containerUrl: String? = null
    private var showFab: Boolean = false
    private var containerId: String? = null
    private var preloadMaxRetry = BuildConfig.PRELOAD_MAX_RETRY
    private var containerUrlChanged = false
    private var replacementUrl = ""
    private var main: Containable? = null
    private var preloadRetryCount = 0
    lateinit var floatingActionMenu: FloatingActionMenu

    companion object {
        val TAG = ContainerFragment::class.java.simpleName
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        inflater.inflate(R.layout.fragment_container, container, false)

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Containable) {
            main = context
        } else {
            throw IllegalArgumentException("Context is not an instance of ${Containable::class.java.simpleName}")
        }
    }

    override fun onDetach() {
        super.onDetach()
        main = null
    }

    @SuppressLint("AddJavascriptInterface")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        containerUrl = arg.url
        showFab = arg.showFab
        containerId = arg.containerId
        preloadMaxRetry = arg.preloadMaxRetry
        preloadRetryCount = 0
        Log.d(TAG, "container created with url $containerUrl")

        CookieManager.getInstance().setAcceptCookie(true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(wv_container, true)
        }

        layout_menu?.also {
            if (showFab) {
                it.visibility = View.VISIBLE
                setupFab()
            } else {
                it.visibility = View.GONE
            }
        }

        wv_container.apply {
            setBackgroundColor(Color.TRANSPARENT)
            settings.apply {
                javaScriptEnabled = true
                pluginState = WebSettings.PluginState.ON
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    mediaPlaybackRequiresUserGesture = false
                }
                javaScriptCanOpenWindowsAutomatically = false
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
                }
                builtInZoomControls = false
                loadWithOverviewMode = true
                setRenderPriority(WebSettings.RenderPriority.HIGH)
                useWideViewPort = true
                setSupportMultipleWindows(false)
                setSupportZoom(false)
                domStorageEnabled = true
                allowFileAccess = true
                allowFileAccessFromFileURLs = true
                allowUniversalAccessFromFileURLs = true
                setAppCacheEnabled(true)
                cacheMode = WebSettings.LOAD_DEFAULT
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    setRendererPriorityPolicy(WebView.RENDERER_PRIORITY_IMPORTANT, false)
                }
            }
            webViewClient = object : WebViewClient() {
                override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                    super.onPageStarted(view, url, favicon)
                    if (containerUrl.equals(url, true).not()) {
                        Log.d(TAG, "$containerId container url changed: $url from: $containerUrl")
                        containerUrlChanged = true
                        preloadRetryCount = 0
                    }
                }

                @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest): Boolean {
                    if (request.url.toString().startsWith(
                            "tel:",
                            "sms:",
                            "smsto:",
                            "mms:",
                            "mmsto:",
                            "mailto:"
                        )
                    ) {
                        activity?.startActivity(Intent(Intent.ACTION_VIEW, request.url.toString().toUri()))
                        return true
                    } else if (request.url.toString().startsWith("skype:")) {
                        if (activity?.isSkypeClientInstalled()!!) {
                            activity?.launchSkype(request.url.toString())
                        } else {
                            activity?.goToSkypeInPlaystore()
                        }
                        return true
                    }
                    return false
                }

                override fun shouldOverrideUrlLoading(view: WebView?, url: String): Boolean {
                    if (url.startsWith("tel:", "sms:", "smsto:", "mms:", "mmsto:", "mailto:")) {
                        activity?.startActivity(Intent(Intent.ACTION_VIEW, url.toUri()))
                        return true
                    } else if (url.startsWith("skype:")) {
                        if (activity?.isSkypeClientInstalled()!!) {
                            activity?.launchSkype(url)
                        } else {
                            activity?.goToSkypeInPlaystore()
                        }
                        return true
                    }
                    return false
                }

                override fun onReceivedHttpError(
                    view: WebView?,
                    request: WebResourceRequest?,
                    errorResponse: WebResourceResponse?
                ) {
                    super.onReceivedHttpError(view, request, errorResponse)
                    wv_container?.setBackgroundColor(Color.WHITE)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && containerUrl == request?.url.toString()) {
                        Log.d(TAG, "$containerId: onreceivedhttperror")
                        attemptReloadUrl()
                    }
                }

                override fun onReceivedSslError(view: WebView?, handler: SslErrorHandler?, error: SslError?) {
                    super.onReceivedSslError(view, handler, error)
                    wv_container?.setBackgroundColor(Color.WHITE)
                    Log.d(TAG, "$containerId: onreceivedsslerror")
                    attemptReloadUrl()
                }

                override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
                    super.onReceivedError(view, request, error)
                    wv_container?.setBackgroundColor(Color.WHITE)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && containerUrl == request?.url.toString()) {
                        Log.d(TAG, "$containerId: onreceivederror")
                        attemptReloadUrl()
                    }
                }

                override fun onReceivedError(
                    view: WebView?,
                    errorCode: Int,
                    description: String?,
                    failingUrl: String?
                ) {
                    super.onReceivedError(view, errorCode, description, failingUrl)
                    if (containerUrl == failingUrl.toString()) {
                        Log.d(TAG, "$containerId: onreceivederror<21")
                        attemptReloadUrl()
                    }
                }
                override fun onPageFinished(view: WebView?, url: String?) {
                    super.onPageFinished(view, url)
                    Log.d(TAG, "$containerId: onpagefinished: $url")
                    main?.onContainerReady(containerId)
                }
            }
            addJavascriptInterface(object : MobileJSBindingInterface() {

                @JavascriptInterface
                override fun onSlotGameClosed() {
                }

                @JavascriptInterface
                override fun closeContainerAndRedirect(path: String) {
                    main?.closeContainerAndRedirect(path)
                }

                @JavascriptInterface
                override fun executeCommand(command: String) {
                    main?.executeCommand(command)
                }


                @JavascriptInterface
                override fun nativeBack() {
                    main?.onBackPressed()
                }

                @JavascriptInterface
                override fun onSubmitDeposit(url: String) {
                    activity?.startActivityForResult(
                        context.intentFor<WebActivity>(WebActivity.EXTRA_URL to url),
                        MainActivity.REQUEST_CODE_WEBACTIVITY
                    )
                }

                @JavascriptInterface
                override fun openExternalBrowser(url: String) {
                    Log.d(TAG, "opening in external browser: $url")
//                    activity?.browse(url)
                    activity?.browseExternal(url)
                }

                @JavascriptInterface
                override fun openApp(packageId: String, downloadUrl: String) {
                    with(context.packageManager.getLaunchIntentForPackage(packageId)) {
                        if (this != null) {
                            context.startActivity(this)
                        } else {
                            downloadUrl.let {
                                //                                activity?.browse(it)
                                activity?.browseExternal(url)
                            }
                        }
                    }
                }

                @JavascriptInterface
                override fun onNativeBackPressed() {
                }
            }, "Native")

            if (replacementUrl.isEmpty()) {
                loadUrl(containerUrl)
            } else {
                loadUrl(replacementUrl)
            }
        }
    }

    private fun setupFab() {
        val itemBuilder = SubActionButton.Builder(activity)
        val homeButton = itemBuilder.setContentView(ImageView(activity).apply {
            setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_fab_home))
        }).build().apply {
            setOnClickListener {
                floatingActionMenu.close(true)
                main?.onBackPressed()
            }
        }
        val backButton = itemBuilder.setContentView(ImageView(activity).apply {
            setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_fab_back))
        }).build().apply {
            setOnClickListener {
                floatingActionMenu.close(true)
                main?.onBackPressed()
            }
        }
        val cashierButton = itemBuilder.setContentView(ImageView(activity).apply {
            setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_fab_cashier))
        }).build().apply {
            setOnClickListener {
                floatingActionMenu.close(true)
                main?.closeContainerAndRedirect("window.redirectTo('/funds');")
            }
        }

        floatingActionMenu = FloatingActionMenu.Builder(activity).setStartAngle(248)
            .setEndAngle(112)
            .setRadius(46f.dpToPx())
            .addSubActionView(homeButton)
            .addSubActionView(backButton)
            .addSubActionView(cashierButton)
            .attachTo(img_arrow)
            .setStateChangeListener(object : FloatingActionMenu.MenuStateChangeListener {
                override fun onMenuOpened(menu: FloatingActionMenu?) {
                    img_glow.visibility = View.VISIBLE
                    layout_dim.visibility = View.VISIBLE
                }

                override fun onMenuClosed(menu: FloatingActionMenu?) {
                    img_glow.visibility = View.INVISIBLE
                    layout_dim.visibility = View.GONE
                }
            }).build()

        img_arrow.setOnTouchListener(object : View.OnTouchListener {
            private val CLICK_DRAG_TOLERANCE = 10f
            private var downRawX: Float = 0.toFloat()
            private var downRawY: Float = 0.toFloat()
            private var dX: Float = 0.toFloat()
            private var dY: Float = 0.toFloat()

            override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {
                if (img_glow.getVisibility() == View.VISIBLE) {
                    return false
                }

                val action = motionEvent.action
                if (action == MotionEvent.ACTION_DOWN) {

                    downRawX = motionEvent.rawX
                    downRawY = motionEvent.rawY
                    dX = view.x - downRawX
                    dY = view.y - downRawY

                    return true // Consumed
                } else if (action == MotionEvent.ACTION_MOVE) {

                    val viewWidth = view.width
                    val viewHeight = view.height

                    val viewParent = view.parent as View
                    val parentWidth = viewParent.width
                    val parentHeight = viewParent.height

                    var newX = motionEvent.rawX + dX
                    newX = Math.max(0f, newX) // Don't allow the FAB past the left hand side of the parent
                    newX = Math.min(
                        (parentWidth - viewWidth).toFloat(),
                        newX
                    ) // Don't allow the FAB past the right hand side of the parent

                    var newY = motionEvent.rawY + dY
                    newY = Math.max(0f, newY) // Don't allow the FAB past the top of the parent
                    newY = Math.min(
                        (parentHeight - viewHeight).toFloat(),
                        newY
                    ) // Don't allow the FAB past the bottom of the parent

                    img_glow.animate()
                        .y(newY).setDuration(0).start()

                    view.animate()
                        .y(newY).setDuration(0).start()

                    return true // Consumed
                } else if (action == MotionEvent.ACTION_UP) {

                    val upRawX = motionEvent.rawX
                    val upRawY = motionEvent.rawY

                    val upDX = upRawX - downRawX
                    val upDY = upRawY - downRawY

                    if (Math.abs(upDX) < CLICK_DRAG_TOLERANCE && Math.abs(upDY) < CLICK_DRAG_TOLERANCE) { // A click
                        view.performClick()
                        return true
                    } else { // A drag
                        return true // Consumed
                    }
                } else {
                    return activity?.onTouchEvent(motionEvent)!!
                }
            }
        })

        img_arrow.setOnClickListener {
            if (floatingActionMenu.isOpen) {
                floatingActionMenu.close(true)
            } else {
                floatingActionMenu.open(true)
            }
        }

        layout_dim.onClick { img_arrow.performClick() }
    }

    fun reinitWithUrl(url: String) {
        if (wv_container == null) {
            replacementUrl = url
            containerUrl = replacementUrl
        } else {
            wv_container.loadUrl("about:blank")
            containerUrl = url
            wv_container.loadUrl(url)
            preloadRetryCount = 0
        }
        containerUrlChanged = false
    }

    fun onContainerHidden() {
        if (containerUrlChanged) {
            if (containerUrl != null && containerUrl!!.isEmpty().not()) {
                wv_container.loadUrl(containerUrl)
            }
            containerUrlChanged = false
        }
    }

    private fun attemptReloadUrl() {
        if (preloadRetryCount < preloadMaxRetry) {
            wv_container.loadUrl(containerUrl)
            preloadRetryCount++
        }
        else {
            Log.e(TAG, "$containerId reload attempt exceeded retry: $preloadRetryCount")
        }
    }

}
