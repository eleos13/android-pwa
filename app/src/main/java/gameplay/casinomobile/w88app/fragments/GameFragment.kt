package gameplay.casinomobile.w88app.fragments

import android.annotation.SuppressLint
import android.app.DownloadManager
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.net.http.SslError
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import androidx.annotation.RequiresApi
import androidx.core.net.toUri
import gameplay.casinomobile.w88app.*
import gameplay.casinomobile.w88app.activities.GameActivity
import gameplay.casinomobile.w88app.customviews.FloatingActionMenu
import gameplay.casinomobile.w88app.data.GameArgs
import gameplay.casinomobile.w88app.interfaces.Containable
import gameplay.casinomobile.w88app.interfaces.MobileJSBindingInterface
import kotlinx.android.synthetic.main.activity_game.*
import org.jetbrains.anko.support.v4.toast
import org.jetbrains.anko.toast

class GameFragment : BaseFragment<GameArgs>() {
    override fun getParentView() = parentPanel

    companion object {
        val TAG = GameFragment::class.java.simpleName
    }

    override fun connectionLost() {
        try {
            wv_game.setBackgroundColor(Color.WHITE)
        } catch (e: Exception) {
        }
    }

    override fun connectionRestored() {
    }

    private var containerUrl: String? = null
    private var showFab = false
    private var main: Containable? = null
    lateinit var floatingActionMenu: FloatingActionMenu

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        inflater.inflate(R.layout.activity_game, container, false)

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Containable) {
            main = context
        } else {
            throw IllegalArgumentException("Context is not an instance of ${Containable::class.java.simpleName}")
        }
    }

    override fun onDetach() {
        super.onDetach()
        main = null
    }

    @SuppressLint("AddJavascriptInterface")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val url = arg.url
        val showFab = arg.showFab
        val forceDashboard = arg.forceDashboard
        val removeNativeBinding = arg.removeNativeBinding

        layout_menu?.also {
            if (showFab) {
                it.visibility = View.VISIBLE
                setupFab()
            } else {
                it.visibility = View.GONE
            }
        }

        wv_game?.apply {
            CookieManager.getInstance().setAcceptCookie(true)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                CookieManager.getInstance().setAcceptThirdPartyCookies(this, true)
            }
            setBackgroundColor(Color.TRANSPARENT)
            settings.apply {
                javaScriptEnabled = true
                pluginState = WebSettings.PluginState.ON
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    mediaPlaybackRequiresUserGesture = false
                }
                javaScriptCanOpenWindowsAutomatically = false
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
                }
                builtInZoomControls = false
                loadWithOverviewMode = true
                setRenderPriority(WebSettings.RenderPriority.HIGH)
                useWideViewPort = true
                setSupportMultipleWindows(false)
                setSupportZoom(false)
                domStorageEnabled = true
                allowFileAccess = true
                allowFileAccessFromFileURLs = true
                allowUniversalAccessFromFileURLs = true
                setAppCacheEnabled(true)
                cacheMode = WebSettings.LOAD_DEFAULT
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    setRendererPriorityPolicy(WebView.RENDERER_PRIORITY_IMPORTANT, false)
                }
            }
            webViewClient = object : WebViewClient() {
                override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                    super.onPageStarted(view, url, favicon)
                    Log.d(GameActivity.TAG, "url: $url")
                    if (showFab &&
                        url != null &&
                        url.isNotEmpty() &&
                        url.toUri().host!!.matches(BuildConfig.defaultDomain.toUri().host!!.toRegex()) &&
                        url.contains("Launcher", true).not()
                    ) {
                        main?.closeContainerAndRedirect(
                            url.replace(
                                "https?://${BuildConfig.defaultDomain.toUri().host}".toRegex(),
                                ""
                            ).also { Log.d(TAG, "redirect command: $it") })
                    }
                }

                @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest): Boolean {
                    if (request.url.toString().startsWith(
                            "tel:",
                            "sms:",
                            "smsto:",
                            "mms:",
                            "mmsto:",
                            "mailto:"
                        )
                    ) {
                        startActivity(Intent(Intent.ACTION_VIEW, request.url.toString().toUri()))
                        return true
                    } else if (request.url.toString().startsWith("skype:")) {
                        if (activity?.isSkypeClientInstalled()!!) {
                            activity?.launchSkype(request.url.toString())
                        } else {
                            activity?.goToSkypeInPlaystore()
                        }
                        return true
                    }
                    return false
                }

                override fun shouldOverrideUrlLoading(view: WebView?, url: String): Boolean {
                    if (url.startsWith("tel:", "sms:", "smsto:", "mms:", "mmsto:", "mailto:")) {
                        startActivity(Intent(Intent.ACTION_VIEW, url.toUri()))
                        return true
                    } else if (url.startsWith("skype:")) {
                        if (activity?.isSkypeClientInstalled()!!) {
                            activity?.launchSkype(url)
                        } else {
                            activity?.goToSkypeInPlaystore()
                        }
                        return true
                    }
                    return false
                }

                override fun onReceivedHttpError(
                    view: WebView?,
                    request: WebResourceRequest?,
                    errorResponse: WebResourceResponse?
                ) {
                    super.onReceivedHttpError(view, request, errorResponse)
                    wv_game?.setBackgroundColor(Color.WHITE)
                }

                override fun onReceivedSslError(view: WebView?, handler: SslErrorHandler?, error: SslError?) {
                    if (handler != null) {
                        handler.proceed()
                    } else {
                        super.onReceivedSslError(view, null, error)
                    }
                    wv_game?.setBackgroundColor(Color.WHITE)
                }

                override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
                    super.onReceivedError(view, request, error)
                    wv_game?.setBackgroundColor(Color.WHITE)
                }
            }
            if (removeNativeBinding.not()) {
                addJavascriptInterface(object : MobileJSBindingInterface() {
                    @JavascriptInterface
                    override fun nativeBack() {
                        main?.onBackPressed()
                    }

                    @JavascriptInterface
                    override fun onSubmitDeposit(url: String) {
                    }

                    @JavascriptInterface
                    override fun openExternalBrowser(url: String) {
                    }

                    @JavascriptInterface
                    override fun openApp(packageId: String, downloadUrl: String) {
                    }

                    @JavascriptInterface
                    override fun onNativeBackPressed() {
                        main?.onBackPressed()
                    }

                    @JavascriptInterface
                    override fun closeContainerAndRedirect(path: String) {
                        main?.closeContainerAndRedirect(path)
                    }

                    @JavascriptInterface
                    override fun onSlotGameClosed() {
                    }

                    @JavascriptInterface
                    override fun executeCommand(command: String) {
                    }
                }, "Native")
            }
            loadUrl(url)
            setDownloadListener { url, userAgent, contentDisposition, mimetype, contentLength ->
                val request = DownloadManager.Request(url.toUri()).apply {
                    setMimeType(mimetype)
                }
                val cookies = CookieManager.getInstance().getCookie(url)
                request.apply {
                    addRequestHeader("cookie", cookies)
                    addRequestHeader("User-Agent", userAgent)
                    setDescription("Download file...")
                    setTitle(URLUtil.guessFileName(url, contentDisposition, mimetype))
                    allowScanningByMediaScanner()
                    setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                    setDestinationInExternalPublicDir(
                        Environment.DIRECTORY_DOWNLOADS,
                        URLUtil.guessFileName(url, contentDisposition, mimetype)
                    )
                }
                val dm = activity?.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
                dm.enqueue(request)
                toast("Downloading file")
                main?.onBackPressed()
            }
        }
    }

    private fun setupFab() {

    }
}